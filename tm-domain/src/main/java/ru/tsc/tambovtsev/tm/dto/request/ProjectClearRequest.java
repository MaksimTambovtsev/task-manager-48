package ru.tsc.tambovtsev.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class ProjectClearRequest extends AbstractUserRequest {

    public ProjectClearRequest(@Nullable String token) {
        super(token);
    }

}

package ru.tsc.tambovtsev.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.dto.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.dto.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.service.IConnectionService;
import ru.tsc.tambovtsev.tm.api.service.dto.ITaskService;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.field.*;
import ru.tsc.tambovtsev.tm.dto.model.TaskDTO;
import ru.tsc.tambovtsev.tm.repository.dto.ProjectRepository;
import ru.tsc.tambovtsev.tm.repository.dto.TaskRepository;

import javax.persistence.EntityManager;
import java.util.*;

public class TaskService extends AbstractUserOwnedService<TaskDTO, ITaskRepository> implements ITaskService {

    public TaskService(
            @Nullable final ITaskRepository repository,
            @NotNull IConnectionService connection
    ) {
        super(repository, connection);
    }

    @NotNull
    @Override
    public ITaskRepository getRepository(@NotNull EntityManager entityManager) {
        return new TaskRepository(entityManager);
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        try {
            @Nullable final TaskDTO task = repository.findById(userId, id);
            if (task == null) return null;
            entityManager.getTransaction().begin();
            task.setName(name);
            task.setDescription(description);
            task.setUserId(userId);
            repository.updateById(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        try {
            final TaskDTO task = repository.findById(userId, id);
            if (task == null) return null;
            entityManager.getTransaction().begin();
            task.setStatus(status);
            task.setUserId(userId);
            repository.updateById(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(
            @Nullable final String userIdPr,
            @Nullable final Sort sort
    ) {
        Optional.ofNullable(userIdPr).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(sort).orElseThrow(NameEmptyException::new);
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        try {
            return repository.findAll(userIdPr, sort);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll() {
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        try {
            return repository.findAllTask();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void addAll(@NotNull final Collection<TaskDTO> models) {
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        try {
            @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.addAll(models);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Collection<TaskDTO> set(@NotNull final Collection<TaskDTO> models) {
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        try {
            @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clearTask();
            repository.addAll(models);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return models;
    }

    @Override
    public void clearTask() {
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        try {
            @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clearTask();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}

package ru.tsc.tambovtsev.tm.repository;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.tsc.tambovtsev.tm.api.repository.dto.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.dto.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.repository.dto.IUserRepository;
import ru.tsc.tambovtsev.tm.api.service.IConnectionService;
import ru.tsc.tambovtsev.tm.api.service.IPropertyService;
import ru.tsc.tambovtsev.tm.api.service.dto.IProjectService;
import ru.tsc.tambovtsev.tm.api.service.dto.IUserService;
import ru.tsc.tambovtsev.tm.dto.model.ProjectDTO;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.repository.dto.ProjectRepository;
import ru.tsc.tambovtsev.tm.repository.dto.UserRepository;
import ru.tsc.tambovtsev.tm.service.ConnectionService;
import ru.tsc.tambovtsev.tm.service.PropertyService;
import ru.tsc.tambovtsev.tm.service.dto.ProjectService;
import ru.tsc.tambovtsev.tm.service.dto.UserService;
import ru.tsc.tambovtsev.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectRepositoryTest {

    private static IPropertyService propertyService;

    private static IConnectionService connectionService;

    private static EntityManager entityManager;

    @NotNull
    private IProjectRepository projectRepository;

    @NotNull
    private ITaskRepository taskRepository;

    @NotNull
    private IUserRepository userRepository;

    @Nullable
    private IProjectService projectService;

    @NotNull
    private IUserService userService;

    @Nullable
    private UserDTO user = new UserDTO();

    @BeforeClass
    public static void initConnection() {
        propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
        entityManager = connectionService.getEntityManager();
    }

    @AfterClass
    public static void disconnectConnection() {
        entityManager.close();
        connectionService.close();
    }

    @Before
    public void setProjectRepository() {
        projectRepository = new ProjectRepository(entityManager);
        userRepository =new UserRepository(entityManager);
        projectService = new ProjectService(projectRepository, connectionService);
        userService = new UserService(
                connectionService, userRepository,  propertyService, projectRepository, taskRepository
        );
        @Nullable ProjectDTO project = new ProjectDTO();
        user.setEmail("test@test.ru");
        user.setLogin("test");
        user.setPasswordHash(HashUtil.salt(propertyService, "test"));
        userService.create(user);
        project.setUserId(user.getId());
        project.setName("123");
        project.setDescription("432");
        projectService.create(project);
        project = new ProjectDTO();
        project.setUserId(user.getId());
        project.setName("1321");
        project.setDescription("234");
        projectService.create(project);
    }

    @After
    public void clearProjectRepository() {
        projectService.clearProject();
    }

    @Test
    public void testFindAll() {
        Assert.assertFalse(projectRepository.findAll(user.getId(), Sort.NAME).isEmpty());
    }

    @Test
    public void testFindAllNegative() {
        Assert.assertFalse(projectRepository.findAll(user.getId(), Sort.NAME).isEmpty());
    }

    @Test
    public void testFindById() {
        @Nullable final List<ProjectDTO> projects = projectRepository.findAll(user.getId(), Sort.NAME);
        @Nullable final String projectId = projects.stream().findFirst().get().getId();
        Assert.assertFalse(projectRepository.findById(projectId).getName().isEmpty());
    }

    @Test
    public void testRemoveById() {
        @NotNull final ProjectDTO project = projectRepository.findAll(user.getId(), Sort.NAME).get(1);
        entityManager.getTransaction().begin();
        projectRepository.removeById(project.getId());
        entityManager.getTransaction().commit();
        Assert.assertNotEquals(project.getId(), projectRepository.findAll(user.getId(), Sort.NAME).get(0).getId());
    }

    @Test
    public void testUpdateById() {
        @NotNull final ProjectDTO project = projectRepository.findAll(user.getId(), Sort.NAME).get(1);
        project.setName("333-333-333-333");
        entityManager.getTransaction().begin();
        projectRepository.updateById(project);
        entityManager.getTransaction().commit();
        Assert.assertEquals(projectRepository.findById(project.getId()).getName(), "333-333-333-333");
    }

    @Test
    public void testClear() {
        entityManager.getTransaction().begin();
        projectRepository.clearProject();
        entityManager.getTransaction().commit();
        Assert.assertTrue(projectRepository.findAll(user.getId(), Sort.NAME).isEmpty());
    }

}
